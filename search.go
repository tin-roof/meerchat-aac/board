package board

import (
	"context"
	"fmt"

	"github.com/google/uuid"
	"gitlab.com/tin-roof/meerchat-aac/database"
	"gitlab.com/tin-roof/meerchat-aac/database/query"
	"gitlab.com/tin-roof/meerchat-aac/logger"
)

// SearchParams define the details of the search
type SearchParams struct {
	Keyword        string
	Profile        *uuid.UUID
	User           *uuid.UUID
	Limit          int
	Page           int
	Order          query.Order
	GetTotalBoards bool
}

// Search uses params to find matching boards in the db
func Search(ctx context.Context, db database.DB, params SearchParams) ([]*Board, int, error) {
	ctx, log := logger.New(ctx, "board.Search")

	log.Info(ctx, "searching for boards")

	// @TODO
	query, queryParams, err := buildSearchQuery(ctx, params)
	if err != nil {
		log.WithError(err).Info(ctx, "failed to build board search query")
		return nil, 0, err
	}

	// lookup the boards
	rows, err := db.Conn.Query(ctx, query, queryParams...)
	if err != nil {
		log.WithError(err).
			WithField("query", query).
			WithField("params", queryParams).
			Info(ctx, "error running board search lookup query")
		return nil, 0, err
	}

	// build the board list
	var totalBoards int
	var boards []*Board
	for rows.Next() {
		var b Board
		if err := rows.Scan(
			&b.ID,
			&b.User,
			&b.Title,
			&b.Buttons,
			&b.Grid,
			&b.Published,
			&b.Description,
			&totalBoards,
		); err != nil {
			log.WithError(err).Info(ctx, "error scanning board lookup query row")
			continue
		}

		boards = append(boards, &b)
	}

	return boards, totalBoards, nil
}

func buildSearchQuery(ctx context.Context, params SearchParams) (string, []interface{}, error) {
	ctx, _ = logger.New(ctx, "board.buildSearchQuery")

	// @TODO: make sure the params are all defined properly to make the query

	keywordOrder := "$2"
	queryParams := []interface{}{params.User.String()}
	filterString := `p."user" = $1`
	getTotalBoards := "0"

	// add the profile filter
	if params.Profile != nil {
		filterString = filterString + ` OR p."user" = $2`
		queryParams = append(queryParams, params.Profile.String())
		keywordOrder = "$3"
	}

	if params.GetTotalBoards {
		getTotalBoards = "count(*) OVER()"
	}

	// add the keyword to the query params
	queryParams = append(queryParams, params.Keyword)

	query := fmt.Sprintf(
		`SELECT 
			p."id",
			p."user",
			p."title",
			p."buttons",
			p."grid",
			p."published",
			p."description",
			%s AS total
		FROM "page" p
		WHERE 
			p."deleted" = FALSE
			AND (%s OR p."published" = TRUE)
			AND (p."title" LIKE '%c' || %s || '%c' OR p."description" LIKE '%c' || %s || '%c')
		ORDER BY p."%s" %s
		LIMIT %v OFFSET %v;`,
		getTotalBoards,
		filterString,
		'%',
		keywordOrder,
		'%',
		'%',
		keywordOrder,
		'%',
		params.Order.Field,
		params.Order.Direction,
		params.Limit,
		query.CalculateOffset(params.Page, params.Limit),
	)

	return query, queryParams, nil
}
