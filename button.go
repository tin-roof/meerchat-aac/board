package board

import (
	_ "embed"
)

//go:embed defaults/home_board_buttons.json
var DefaultBoardButtons []byte

type Button struct {
	AddToPhrase     bool   `json:"addToPhrase"`               // should the button label or speech be added to the phrase or not
	BackgroundColor string `json:"backgroundColor,omitempty"` // background color for the button
	BorderColor     string `json:"borderColor,omitempty"`     // border color for the button
	ButtonID        string `json:"buttonID"`                  // id for the button - notes which space it goes to on the board
	Hidden          bool   `json:"hidden"`                    // is the button visible or not
	Image           string `json:"image,omitempty"`           // link to image of the button
	ImagePosition   string `json:"imagePosition,omitempty"`   // position of the image on the button - @TODO: not implemented yet
	ImageSize       string `json:"imageSize,omitempty"`       // how big is the image of the button - @TODO: not implemented yet
	Label           string `json:"label,omitempty"`           // label for the button - text that shows up on the board
	LabelPosition   string `json:"labelPosition,omitempty"`   // position of the label on the button - @TODO: not implemented yet
	Link            string `json:"link,omitempty"`            // link to board or external url
	OpenLink        bool   `json:"openLink"`                  // is the button a folder/link
	SayNow          bool   `json:"sayNow"`                    // should the speech/label for the button be spoken on click
	Speech          string `json:"speech,omitempty"`          // words to be spoken or added to phrase - default is just the label
	TextColor       string `json:"textColor,omitempty"`       // font color on the button
	TextSize        string `json:"textSize,omitempty"`        // how big is the text of the button - @TODO: not implemented yet
}
