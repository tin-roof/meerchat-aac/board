-- mark a board as deleted so that it wont show up in queries or searches
-- but will still be available to direct lookups since it could be linked to from buttons already
UPDATE "page" SET "deleted" = true WHERE "id" = $1;