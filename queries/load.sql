-- lookup a page by its id
SELECT 
    p."created_at",
    p."updated_at",
    p."user", 
    p."title", 
    p."buttons", 
    p."grid", 
    p."published", 
    p."description", 
    p."deleted"
FROM 
    "page" p
WHERE
    p."id" = $1;