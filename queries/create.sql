-- create a new page for the user
INSERT INTO "page" ("user", "title", "buttons", "grid", "published", "description", "deleted") 
VALUES($1, $2, $3, $4, $5, $6, $7) 
RETURNING "id"; 