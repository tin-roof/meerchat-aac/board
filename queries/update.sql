-- update the fields for given button
UPDATE "page" 
SET 
    "updated_at" = CURRENT_TIMESTAMP,
    "user" = $1, 
    "title" = $2, 
    "buttons" = $3, 
    "grid" = $4, 
    "published" = $5, 
    "description" = $6, 
    "deleted" = $7 
WHERE "id" = $8;