package board

import (
	"context"
	_ "embed"
	"time"

	"github.com/google/uuid"
	"gitlab.com/tin-roof/meerchat-aac/database"
	"gitlab.com/tin-roof/meerchat-aac/logger"
)

// queries needed for modifying boards
var (
	//go:embed queries/create.sql
	createQuery string

	//go:embed queries/delete.sql
	deleteQuery string

	//go:embed queries/load.sql
	loadQuery string

	//go:embed queries/update.sql
	updateQuery string
)

// Board defines the structure and settings of a board
type Board struct {
	Buttons     []Button   `json:"buttons"`             // list of buttons on a board
	CreatedAt   *time.Time `json:"createdAt,omitempty"` // the date the board was created
	Deleted     bool       `json:"-"`                   // if the board is deleted or not
	Description string     `json:"description"`         // user defined description of the board
	Grid        int        `json:"grid"`                // grid size on the board
	ID          *uuid.UUID `json:"id"`                  // id of the board
	Published   bool       `json:"published"`           // if the board is published or not
	Title       string     `json:"title"`               // user defined title of the board
	UpdatedAt   *time.Time `json:"updatedAt,omitempty"` // date the board was last updated
	User        *uuid.UUID `json:"user"`                // user id of the user/profile that created the board

	loaded bool `json"-"` // if the board has been loaded from the db or not
}

// Delete marks a board as deleted
func (b *Board) Delete(ctx context.Context, db database.DB) error {
	ctx, log := logger.New(ctx, "board.Board.Delete", logger.WithField("id", b.ID))

	log.Info(ctx, "deleting a board")

	if _, err := db.Conn.Exec(
		ctx,
		deleteQuery,
		b.ID,
	); err != nil {
		log.WithError(err).Info(ctx, "failed to delete board")
		return err
	}

	return nil
}

// Load uses a provided id to get the details for a board from the DB
func (b *Board) Load(ctx context.Context, db database.DB) error {
	ctx, log := logger.New(ctx, "board.Board.Load", logger.WithField("id", b.ID))

	log.Info(ctx, "loading a board")

	// lookup the board by ID
	if err := db.Conn.QueryRow(ctx, loadQuery, b.ID).Scan(
		&b.CreatedAt,
		&b.UpdatedAt,
		&b.User,
		&b.Title,
		&b.Buttons,
		&b.Grid,
		&b.Published,
		&b.Description,
		&b.Deleted,
	); err != nil {
		log.WithError(err).Info(ctx, "failed to load board")
		return err
	}

	return nil
}

// Save updates/creates the board in the DB
func (b *Board) Save(ctx context.Context, db database.DB) error {
	ctx, log := logger.New(ctx, "board.Board.Save")

	// update if the board already has an ID
	if b.ID == nil {
		log.Info(ctx, "saving a new board")

		// create a new board and set the ID
		if err := db.Conn.QueryRow(
			ctx,
			createQuery,
			b.User,
			b.Title,
			b.Buttons,
			b.Grid,
			b.Published,
			b.Description,
			b.Deleted,
		).Scan(&b.ID); err != nil {
			log.WithError(err).Info(ctx, "failed to create board")
			return err
		}

		return nil
	}

	log.WithField("id", b.ID).Info(ctx, "saving a board")

	if _, err := db.Conn.Exec(
		ctx,
		updateQuery,
		b.User,
		b.Title,
		b.Buttons,
		b.Grid,
		b.Published,
		b.Description,
		b.Deleted,
		b.ID,
	); err != nil {
		log.WithError(err).Info(ctx, "failed to update board")
		return err
	}

	return nil
}
